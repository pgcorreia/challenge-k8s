Changes to the voting app:

1. Postgres Changes
Changed postgres image -> wrong image will lead to a pullback error, image needs to be fixed
  - Possible solutions for this:
    - Correct the yaml
    - Using `kubectl edit` to update the yaml file or other command

If I change the port of postgres, the result-app won't be able to connect to db

**Correct postgres image**

2. Redis Changes
Changed redis port -> redis pod will launch and run but it will break the app.

By inspecting the pod status with `kubectl get pods` we can see that all pods are up and running

We can troubleshoot this by:
- `kubectl logs voting-app-pod` -> WIll have the error msg   `File "/usr/local/lib/python3.9/site-packages/redis/connection.py", line 598, in connect`

- `kubectl logs worker-app-pod` -> Will keep trying to connect to redis

**Correct redis port**


This can open discussion on how to update deploys in k8s. If we update the live manifest, it will diverge from the yaml used for deploy.

When using ArgoCD, it will always look at the yaml in repo. If we only change using `kubectl edit` and yaml diverges from repo, ArgoCD will revert

3. Mismatch in labels of result-app-deploy -> will throw an error and wont create the deploy
  - Correct yaml

Worker App will only work if services are deployed - probably hardcoded something inside the container 



Questions for Interview
1. How to scale a deployment?