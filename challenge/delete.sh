#!/bin/sh

# ### Delete Stuff
deployments=$(kubectl -n challenge  get deployments -o name)

for deployment in $deployments; do
    echo "Deleting deployment: $deployment"
    kubectl -n challenge delete $deployment
done

services=$(kubectl -n challenge get services -o name)

for service in $services; do
    echo "Deleting deployment: $service"
    kubectl -n challenge delete $service
done
