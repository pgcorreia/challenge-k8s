#!/bin/bash

# ### Deploy Namespace
kubectl apply -f ./00-namespace/namespace.yaml

# ### Deploy stuff 
deploys="./01-deployments"

for file in "$deploys"/*.yaml; do
    if [ -f "$file" ]; then
        echo "Applying $file..."
        kubectl apply -f "$file"
        echo "Done."
    fi
done

# ### Deploy stuff 
svc="./02-services"

for file in "$svc"/*.yaml; do
    if [ -f "$file" ]; then
        echo "Applying $file..."
        kubectl apply -f "$file"
        echo "Done."
    fi
done
