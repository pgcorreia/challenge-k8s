#!/bin/bash

### Deploy stuff 
deploys="./deployments"

for file in "$deploys"/*.yaml; do
    if [ -f "$file" ]; then
        echo "Applying $file..."
        kubectl apply -f "$file"
        echo "Done."
    fi
done

### Deploy stuff 
svc="./services"

for file in "$svc"/*.yaml; do
    if [ -f "$file" ]; then
        echo "Applying $file..."
        kubectl apply -f "$file"
        echo "Done."
    fi
done
