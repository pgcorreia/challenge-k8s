#!/bin/sh

# ### Delete Stuff
deployments=$(kubectl get deployments -o name)

for deployment in $deployments; do
    echo "Deleting deployment: $deployment"
    kubectl delete $deployment
done

services=$(kubectl get services -o name)

for service in $services; do
    echo "Deleting deployment: $service"
    kubectl delete $service
done
