# Challenge Overview

The challenge is based on a simple distributed application running across multiple Pods.

The application consists in a voting app and has the following diagram:

![architecture](/images/k8s-challenge-architechture.png)

Let's breakdown each Pod of our distributed application.


### Voting App

The Python-based voting app is designed to restrict each client's browser to a single vote. After submitting a vote, the app will send the result to the Redis database.

When you access the voting app through your browser, you can expect the following output:

![vote-output](/images/vote.JPG)

### Redis

Will be used for messaging. Data on redis will be processed by the worker Pod.

### Woker Pod

It will fetch the votes from redis and store them in Postgres.

### Postgres

Postgres is used for storage.

### Result App

The Result App, will display the voting results by retrieving votes from the Postgres database. The results are then presented in the browser with the following output:

![result-output](/images/result.JPG)

## Fix the App Challenge

## Run the app in Kubernetes

You can us





## Expected Output



## Submission