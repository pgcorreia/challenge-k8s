To purge minikube - `minikube delete --all`

Start minikube - `minikube start`

Get endpoints - `minikube service <service-name> --url`